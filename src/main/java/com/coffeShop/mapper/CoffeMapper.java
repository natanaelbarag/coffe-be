package com.coffeShop.mapper;

import com.coffeShop.model.CoffeOrder;
import com.coffeShop.model.Country;
import com.coffeShop.repository.CoffeTypeRepository;
import com.coffeShop.restApi.dto.CoffeOrderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CoffeMapper {

	@Autowired
	CoffeTypeRepository coffeTypeRepository;

	@Autowired
	private CoffeTypeMapper coffeTypeMapper;

	public CoffeOrder asCoffeEntity(CoffeOrderDTO coffeOrderDTO) {
		CoffeOrder coffeOrder = new CoffeOrder();
		coffeOrder.setCountry(Country.valueOf(coffeOrderDTO.getCountry()));
		coffeOrder.setName(coffeOrderDTO.getName());
		coffeOrder.setPrice(coffeOrderDTO.getPrice());
		coffeOrder.setType(coffeTypeRepository.getOne(coffeOrderDTO.getCoffeType().getId()));
		coffeOrder.setQuantity(coffeOrderDTO.getQuantity());
		return coffeOrder;
	}

	public CoffeOrderDTO asCoffeDTO(CoffeOrder coffeOrder) {

		return CoffeOrderDTO.builder()
				.id(coffeOrder.getId())
				.name(coffeOrder.getName())
				.country(coffeOrder.getCountry().toString())
				.coffeType(coffeTypeMapper.asCoffeTypeDTO(coffeOrder.getType()))
				.quantity(coffeOrder.getQuantity())
				.price(coffeOrder.getPrice())
				.build();
	}

}
