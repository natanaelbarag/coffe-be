package com.coffeShop.mapper;

import com.coffeShop.model.CoffeType;
import com.coffeShop.restApi.dto.CoffeTypeDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CoffeTypeMapper {

	@Autowired
	private ModelMapper modelMapper;

	public CoffeType asCoffeTypeEntity(CoffeTypeDTO coffeTypeDTO) {
		CoffeType coffeType = modelMapper.map(coffeTypeDTO, CoffeType.class);
		return coffeType;
	}

	public CoffeTypeDTO asCoffeTypeDTO(CoffeType coffeType) {
		CoffeTypeDTO coffeTypeDTO = modelMapper.map(coffeType, CoffeTypeDTO.class);
		return coffeTypeDTO;
	}
}
