package com.coffeShop.repository;

import com.coffeShop.model.CoffeOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoffeRepository extends JpaRepository<CoffeOrder, Integer> {
}
