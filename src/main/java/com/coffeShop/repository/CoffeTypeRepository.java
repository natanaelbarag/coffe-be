package com.coffeShop.repository;

import com.coffeShop.model.CoffeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoffeTypeRepository extends JpaRepository<CoffeType, Integer> {
}
