package com.coffeShop.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "coffe_order")
public class CoffeOrder extends BaseCoffeEntity {

	@ManyToOne
	private CoffeType type;

	@Column(name = "country", nullable = false)
	@Enumerated(EnumType.STRING)
	private Country country;

}
