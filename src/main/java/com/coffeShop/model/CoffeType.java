package com.coffeShop.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "coffe_type")
public class CoffeType extends BaseCoffeEntity {

	@OneToMany(mappedBy = "type")
	private List<CoffeOrder> coffeOrders;

	@Column(name = "discount", nullable = false)
	private double discount;

}
