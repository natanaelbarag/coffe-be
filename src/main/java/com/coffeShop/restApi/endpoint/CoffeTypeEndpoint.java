package com.coffeShop.restApi.endpoint;

import com.coffeShop.restApi.dto.CoffeTypeDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/coffetype")
public interface CoffeTypeEndpoint {

	@PostMapping
	CoffeTypeDTO addCoffeType(@RequestBody CoffeTypeDTO coffeTypeDTO);

	@GetMapping("/{id}")
	CoffeTypeDTO getCoffeType(@PathVariable int id) throws Exception;

	@DeleteMapping("/{id}")
	CoffeTypeDTO deleteCoffeType(@PathVariable int id) throws Exception;

	@PutMapping
	CoffeTypeDTO updateCoffeType(@RequestBody CoffeTypeDTO coffeTypeDTO) throws Exception;

	@GetMapping("/all")
	List<CoffeTypeDTO> getAllCoffeTypes();
}
