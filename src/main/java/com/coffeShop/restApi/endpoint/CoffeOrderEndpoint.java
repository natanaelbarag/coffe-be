package com.coffeShop.restApi.endpoint;

import com.coffeShop.restApi.dto.CoffeOrderDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/coffe")
public interface CoffeOrderEndpoint {

	@PostMapping
	CoffeOrderDTO orderCoffe(@RequestBody CoffeOrderDTO coffeOrderDTO) throws Exception;

	@GetMapping("/allCoffeOrders")
	List<CoffeOrderDTO> getCoffeOrders();
}
