package com.coffeShop.restApi.dto;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CoffeTypeDTO {

	private int id;
	private String name;
	private double discount;
	private double price;
	private double quantity;
}
