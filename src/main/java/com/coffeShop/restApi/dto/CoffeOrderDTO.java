package com.coffeShop.restApi.dto;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CoffeOrderDTO {
	private int id;
	private String name;
	private CoffeTypeDTO coffeType;
	private String country;
	private double price;
	private double quantity;
}
