package com.coffeShop.serviceApi;

import com.coffeShop.model.CoffeOrder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CoffeOrderService {

	CoffeOrder orderCoffe(CoffeOrder coffeOrder) throws Exception;

	List<CoffeOrder> getAllCoffeOrders();
}
