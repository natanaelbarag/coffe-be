package com.coffeShop.serviceApi;

import com.coffeShop.model.CoffeType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CoffeTypeService {
	CoffeType saveCoffeType(CoffeType coffeType);

	CoffeType getCoffeType(int id) throws Exception;

	CoffeType delete(int coffeType) throws Exception;

	CoffeType updateCoffeType(CoffeType coffeType) throws Exception;

	List<CoffeType> getAllCoffeTypes();
}