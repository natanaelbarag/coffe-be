package com.coffeShop.restApiImpl;

import com.coffeShop.mapper.CoffeTypeMapper;
import com.coffeShop.model.CoffeType;
import com.coffeShop.restApi.dto.CoffeTypeDTO;
import com.coffeShop.restApi.endpoint.CoffeTypeEndpoint;
import com.coffeShop.serviceApi.CoffeTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CoffeTypeController implements CoffeTypeEndpoint {

	@Autowired
	private CoffeTypeMapper coffeTypeMapper;

	@Autowired
	private CoffeTypeService coffeTypeService;

	@Override
	public CoffeTypeDTO addCoffeType(CoffeTypeDTO coffeTypeDTO) {
		CoffeType coffeType = coffeTypeMapper.asCoffeTypeEntity(coffeTypeDTO);
		try {
			CoffeType response = coffeTypeService.saveCoffeType(coffeType);
			return coffeTypeMapper.asCoffeTypeDTO(response);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public CoffeTypeDTO getCoffeType(int id) throws Exception {
		try {
			CoffeType response = coffeTypeService.getCoffeType(id);
			return coffeTypeMapper.asCoffeTypeDTO(response);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public CoffeTypeDTO deleteCoffeType(int id) throws Exception {
		try {
			CoffeType response = coffeTypeService.delete(id);
			return coffeTypeMapper.asCoffeTypeDTO(response);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public CoffeTypeDTO updateCoffeType(CoffeTypeDTO coffeTypeDTO) throws Exception {
		CoffeType coffeType = coffeTypeMapper.asCoffeTypeEntity(coffeTypeDTO);
		try {
			CoffeType response = coffeTypeService.updateCoffeType(coffeType);
			return coffeTypeMapper.asCoffeTypeDTO(response);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public List<CoffeTypeDTO> getAllCoffeTypes() {
		try {
			List<CoffeType> coffeTypes = coffeTypeService.getAllCoffeTypes();
			return coffeTypes.stream()
					.map(coffeType -> coffeTypeMapper.asCoffeTypeDTO(coffeType))
					.collect(Collectors.toList());
		} catch (Exception e) {
			throw e;
		}

	}

}
