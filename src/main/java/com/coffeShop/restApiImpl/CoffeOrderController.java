package com.coffeShop.restApiImpl;

import com.coffeShop.mapper.CoffeMapper;
import com.coffeShop.model.CoffeOrder;
import com.coffeShop.restApi.dto.CoffeOrderDTO;
import com.coffeShop.restApi.endpoint.CoffeOrderEndpoint;
import com.coffeShop.serviceApi.CoffeOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CoffeOrderController implements CoffeOrderEndpoint {

	@Autowired
	private CoffeMapper coffeMapper;

	@Autowired
	private CoffeOrderService coffeOrderService;

	@Override
	public CoffeOrderDTO orderCoffe(CoffeOrderDTO coffeOrderDTO) throws Exception {
		CoffeOrder coffeOrder = coffeMapper.asCoffeEntity(coffeOrderDTO);
		try {
			CoffeOrder response = coffeOrderService.orderCoffe(coffeOrder);
			return coffeMapper.asCoffeDTO(response);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<CoffeOrderDTO> getCoffeOrders() {
		try {
			List<CoffeOrder> coffeOrders = coffeOrderService.getAllCoffeOrders();
			return coffeOrders.stream()
					.map(coffe -> coffeMapper.asCoffeDTO(coffe))
					.collect(Collectors.toList());

		} catch (Exception e) {
			throw e;
		}
	}
}
