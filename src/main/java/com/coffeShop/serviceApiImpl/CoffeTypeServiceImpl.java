package com.coffeShop.serviceApiImpl;

import com.coffeShop.model.CoffeType;
import com.coffeShop.repository.CoffeTypeRepository;
import com.coffeShop.serviceApi.CoffeTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CoffeTypeServiceImpl implements CoffeTypeService {

	@Autowired
	private CoffeTypeRepository coffeTypeRepository;

	@Override
	public CoffeType saveCoffeType(CoffeType coffeType) {
		try {
			coffeTypeRepository.save(coffeType);
			return coffeType;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public CoffeType getCoffeType(int id) throws Exception {
		return coffeTypeRepository.findById(id)
				.orElseThrow(() -> new Exception("CoffeType with id " + id + " not found"));

	}

	@Override
	public CoffeType delete(int id) throws Exception {

		CoffeType response =
				coffeTypeRepository.findById(id).orElseThrow(() -> new Exception("CoffeType with id " + id + " not found"));
		coffeTypeRepository.delete(response);
		return response;
	}

	@Override
	public CoffeType updateCoffeType(CoffeType coffeType) throws Exception {
		try {
			CoffeType response =
					coffeTypeRepository.findById(coffeType.getId()).orElseThrow(() -> new Exception("CoffeType with id " + coffeType.getId() + " not found"));

			response.setDiscount(coffeType.getDiscount());
			response.setName(coffeType.getName());
			response.setQuantity(coffeType.getQuantity());
			response.setPrice(coffeType.getPrice());
			coffeTypeRepository.save(response);
			return coffeType;

		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<CoffeType> getAllCoffeTypes() {
		return coffeTypeRepository.findAll();
	}
}
