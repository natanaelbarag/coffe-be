package com.coffeShop.serviceApiImpl;

import com.coffeShop.model.CoffeOrder;
import com.coffeShop.repository.CoffeRepository;
import com.coffeShop.serviceApi.CoffeOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CoffeOrderServiceImpl implements CoffeOrderService {

	@Autowired
	private CoffeRepository coffeRepository;

	@Override
	public CoffeOrder orderCoffe(CoffeOrder coffeOrder) throws Exception {
		try {
			if (coffeQuantityLimitIsNotReached(coffeOrder)) {
				updateAvailableCoffeTypeQuantity(coffeOrder);
				setPriceForCoffeOrder(coffeOrder);
				coffeRepository.save(coffeOrder);
				return coffeOrder;
			} else throw new Exception("Coffe quantity limit excedeed");
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<CoffeOrder> getAllCoffeOrders() {
		return coffeRepository.findAll();
	}

	private void setPriceForCoffeOrder(CoffeOrder coffeOrder) {
		double coffeTypePrice = coffeOrder.getType().getPrice();
		double coffeTypeDiscount = coffeOrder.getType().getDiscount() / 100;
		double coffeOrderQuantity = coffeOrder.getQuantity();
		double price =  coffeTypePrice * coffeOrderQuantity / 100;
		if(coffeTypeDiscount != 0) 	price *= coffeTypeDiscount;

		coffeOrder.setPrice(price);
	}

	private boolean coffeQuantityLimitIsNotReached(CoffeOrder coffeOrder) {
		return coffeOrder.getType().getQuantity() >= coffeOrder.getQuantity();
	}

	private void updateAvailableCoffeTypeQuantity(CoffeOrder coffeOrder) {
		coffeOrder.getType().setQuantity(coffeOrder.getType().getQuantity() - coffeOrder.getQuantity());
	}
}
